{ config, pkgs, lib, home-manager, ... }:

{
  homebrew = {
    enable = true;
    casks = [
      # Development
      "homebrew/cask/docker"

      # Utility
      "linearmouse"
      "balenaetcher"
      "1password-cli"

      # Communication Tools
      "discord"
      "slack"

      # Entertainment
      "spotify"

      # Browsers
      "arc"
    ];

    # These app IDs are from using the mas CLI app
    # mas = mac app store
    # https://github.com/mas-cli/mas
    #
    # $ nix shell nixpkgs#mas
    # $ mas search <app name>
    #
    masApps = {
      "1password" = 1333542190;
      "magnet" = 441258766;
    };
  };
}
