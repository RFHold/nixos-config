{ config, programs, pkgs, lib, ... }:

{
  programs.neovim = {
    enable = true;

    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;

    plugins = with pkgs.vimPlugins; [
      lazy-nvim
    ];

    extraPackages = with pkgs; [];

    extraConfig = ''
      set number relativenumber
      :luafile ~/.config/nvim/lua/init.lua
    '';
  };

  home.file.".config/nvim" = {
    source = ./config;
    recursive = true;
  };
}
