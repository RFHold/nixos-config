{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    # Languages
    go
    rustup
    nodejs_20
  ];
}
