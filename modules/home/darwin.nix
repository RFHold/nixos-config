{ config, pkgs, lib, home-manager, ... }:

let
  user = "robert"; in
{
  # Enable home-manager
  home-manager = {
    useGlobalPkgs = true;
    users.${user} = { pkgs, config, lib, ... }:{
      home = {
	activation = {
	  rsync-home-manager-applications = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
	    rsyncArgs="--archive --checksum --chmod=-w --copy-unsafe-links --delete"
	    apps_source="$genProfilePath/home-path/Applications"
	    moniker="Home Manager Trampolines"
	    app_target_base="${config.home.homeDirectory}/Applications"
	    app_target="$app_target_base/$moniker"
	    mkdir -p "$app_target"
	    ${pkgs.rsync}/bin/rsync $rsyncArgs "$apps_source/" "$app_target"
	  '';
	};

        enableNixpkgsReleaseCheck = false;
        stateVersion = "23.11";

        packages = with pkgs; [
	  # Development
	  docker
	  docker-compose

	  # Media
	  ffmpeg

	  # Fonts
	  (nerdfonts.override { fonts = [ "CascadiaCode" "CodeNewRoman" "FantasqueSansMono" "Iosevka" "ShareTechMono" "Hermit" "JetBrainsMono" "FiraCode" "FiraMono" "Hack" "Hasklig" "Ubuntu" "UbuntuMono" ]; })

	  # Ops
	  opentofu
	  terraform
	  kubernetes-helm
	  packer
	  vault
	  kubectl
	  ansible

	  # Text and terminal utilities
	  htop
	  jq
	  tree
	  unzip
	  wget
	  openssh
	  zip
	  ripgrep
	  libusb1
        ];
      };

      imports = [
        ./git
        ./languages
        ./neovim
        ./ssh
        ./ssh/darwin.nix
        ./zsh
      ];

      # Marked broken Oct 20, 2022 check later to remove this
      # https://github.com/nix-community/home-manager/issues/3344
      manual.manpages.enable = false;

      fonts.fontconfig.enable = true;
    };
  };
}

