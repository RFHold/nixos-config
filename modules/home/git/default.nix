{ config, programs, pkgs, lib, ... }:

let
  name = "rfhold";
  email = "robert@robhold.com"; in
{
  programs.git = {
    enable = true;
    userName = name;
    userEmail = email;
    lfs = {
      enable = true;
    };
    extraConfig = {
      init.defaultBranch = "main";
      core = {
	    editor = "nvim";
        autocrlf = "input";
      };
      pull.rebase = true;
      rebase.autoStash = true;
    };
  };

  home.packages = with pkgs; [
    gitlab-runner
    github-runner
  ];
}
