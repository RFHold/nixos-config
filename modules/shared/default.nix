{ config, pkgs, lib, ... }:

{
  imports = [
    ./cachix
  ];

  nixpkgs = {
    config = {
      allowUnfree = true;
      allowBroken = true;
      allowInsecure = false;
      allowUnsupportedSystem = true;
    };
  };

  environment.systemPackages = with pkgs; [
    # Development
    docker
    docker-compose

    # Media
    ffmpeg

    # Ops
    opentofu
    terraform
    kubernetes-helm
    packer
    vault
    kubectl
    ansible

    # Text and terminal utilities
    htop
    jq
    tree
    unzip
    wget
    openssh
    zip
    ripgrep
  ];
}
